# Prefab Script -UI

## Intro
Ok So, you have bought this asset. You go through everything in this asset. In that case you have probably also found that the UI prefabs are compltely different from 3D and 2D prefabs. In 3D/2D prefabs, you get a tool which is used create any kind of stuff you want. It gives you a clean group of empty game objects, with scripts attached, where you can drag and drop what you want. BUT, this is NOT how the UI Prefabs work. The UI Prefabs consists of prebuilt completed simple to use prefabs, which you can use to furthur beautify and create amazing results!

## Getting Started
You can simply start with going into Prefab Script Tab on the top, and then click on to UI Prefabs, to create a Prefab Script Window for UI.
Now, select on to whatever tools you want. By default, you get
+ Dialog system - simple way to get a conversation started in your game.
+ Game Menu - A simple menu with buttons like pause, play and resume.
+ Main Menu - A simple menu with Play, Exit, and settings buttons
+ Popup - A window/UI element which pops up on conditions, like delay, button press and so on.
+ Object Operations - Operations like destroy or create objects on conditions like delay, etc.
+ Scene Transition - Changes the the current scene in the game with cool animations and effects.
+ Vegnette - a simple tool to add rounded smooth black effects at the borders.

### Making Use of all the tools 
All the tools you see right now in UI Prefab Script is made at it's simplest state. It does not look attractive, not customized and many more. It is not made for that purpose in the first place. It is actually made so that you can use it to create what you want and then finally just change the looks, decorate it furthur before you are completely done.

### Dialog System
With this you get a pretty pale looking completely dialog system. So, if you play this game now, you can see that it has a continue button, which is clicked to go to the next dialog. This is how it works...
+ Go into Dialog System > Canvas > Test Button Game Object
+ You get a script called dialog trigger attached to the Test Button game object
+ Go to Dialog > Sentences option and now you should be able to see all the dialogs
+ Change the number of senteces to how many dialogs you want to add
+ The Dialogs start from element 0. You Can simply type in the dialogs you want
+ You Can also change the name of the person above sentences. By default it is named as "Person"

### Game Menu 
Game menu consists of a pause button which, when clicked opens three buttons with it's own funtions.
+ Pause Button - The pause button allows you to ACTUALLY pause the game and then show the game menu. So you don't have to worry on that.
+ Resume Button - Resume button basically closes the game manu and continues by resuming the game.
+ Menu Button - Menu button is normal button. You can go to Game Menu > Game Menu > BackToMenu GameObject and head over to the MainMenuGameMenu Script attached. There, you can select the index number of whatever scene you want to change to, when clicked on the menu button.
+ Exit - As simple as it seems, exit button simply quits the extire game when clicked on it.

### Main Menu
This is simply the menu which has button... Play, Settings, and exit
+ Play - This button just changes from one scene to another. Go CompleteMenu > play gam object. You can now change the index number of the scene which you want when the button is clicked, through the Play Button Main Menu. You can furthur customize if you want by completely removing the play button and adding a scene transition button(more about it below).
+ Settings - this is simply the same as play button. It is kept here for ease of use.
+ exit - this button quits the whole game/application.

### Object Operations
These help 2 operations and therefore there are two tools here. Destroyer and creator.
+ Destroyer - This is a simple tool which distroy an object specified(drag and drop the object). It would also destroy an object when the game start, on delay, and other conditions
+ Creator - This is a simple tool which creates an object specified(drag and drop the object/Prefab). It would also creaye an object when the game start, on delay, and other conditions

### PopUp
This is a very simple tool which pops up on conditions like - when a button is pressed, on delay, and so on. Again, as mentioned before, it just gives you the basic game object to do it. You can continue to customize it be adding sprites, changing text and so on.

### Scene Transition
This is one of the most usefull tools, scince almost all the games will have some sort of changes in the scenes of the game. It gives you a smooth animated scene change with fade in and out.
Before you click the button in the UI Prefabs window, and add a scene transition tool, notice that there are two of them. One changes the scene when a button is clicked, and the other changes on other conditions like delay, etc. You can use this instead of any button above to change the scene and then add the desired script into the inspector of the button.

### Vignette 
This is a simple effect which many games use. The vignette provided by this tool is very simple and easy to add. After you click and add the vignette tool. You have to go into the inspector, and change the the percentage you desire for a opacity of the vignette. You can now play the game and enjoy. 


## Conclusion
Well... If you have used the asset you would propably realize that the tools does not "look" good. It is really not made for that. It is made for simply made to save your time. Example: It saves time by giving you the whole framework of a menu, but it also encorages you to try out customizing it yourself as well.

If you have furthur problems, 
head hover to FadinGeek Channel where there will be videos posted 

That's about it... 
I hope you enjoy using this asset as much as we enjoyed making it :)
Thank You 
FadinGeek
